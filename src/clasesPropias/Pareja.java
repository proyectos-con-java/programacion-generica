package clasesPropias;

/**
 * @param <T> Objeto Generico
 */

public class Pareja<T>{	
	private T obj;
	
	/**
	 * Establece la variable obj a null;
	 */
	public Pareja(){
		obj = null;
	}
	
	/**
	 * Devuelve el valor del objeto generico obj
	 * @return Devuelve un dato generico
	 */
	public T getObj() {
		return obj;
	}

	/**
	 * Establece el valor del objeto generico obj
	 * @param obj Objeto generico
	 */
	public void setObj(T primero) {
		this.obj = primero;
	}	
}