package clasesPropias;

public class MetodosGenericos {

	public static void main(String[] args) {

		String nom[] = {"a","b","c"};
		int num[] = {1,2,3};
		Integer num2[] = {1,2,3};
		
		System.out.println(MisMatrices.getElementos(num2));
		
	}

}

class MisMatrices{
	
	public MisMatrices(){}
	
	public static <T> String getElementos(T[]a){
		return "El array tiene "+a.length+" elementos";
	}
	
}