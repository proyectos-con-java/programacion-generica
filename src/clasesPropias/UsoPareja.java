package clasesPropias;

public class UsoPareja {
	public static void main(String[] args){
		Pareja<String> p = new Pareja<String>();
		Persona pers1 = new Persona("Ana");
		Pareja<Persona>otra = new Pareja<Persona>();
		
		p.setObj("Dios");
		otra.setObj(pers1);
		
		System.out.println(p.getObj());
		System.out.println(otra.getObj());
	}	
}

class Persona{
	private String nombre;
	
	public Persona(String nombre){
		this.setNombre(nombre);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return nombre;
	}
}